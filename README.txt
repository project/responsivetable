What Is This?
-------------

This module provides integration for the jQuery Tablesaw plugin for creating responsive tables. The Tablesaw plugin allows wide tables to appear as usual on large displays but on smaller screens it can hide certain columns within swiping rows.
Note : It will work for responsive theme 

Installation instructions in Drupal 8
No need to install any other library. just go views responsive formate style setting and choose table format

Installation
-------------


See the README file for detailed installation instructions.
Usage

When creating a view, select the responsive table format.
Click on the Settings link, under the Format section.
Scroll down to the tablesaw Settings section.
First is dropdown has three type responsive table "stack","swipe","toggle".
and also have two check box to enable jquery shorting and also generate dynamic dropdown for responsive table type

Similar Modules

Responsive Tables - You can choose a priority for your columns, and as the screen width is reduced, these columns are hidden completely, which isn't great.
Foo Table - A brilliant module, allowing you to keep all column content, but displays it in block content, rather than keeping the original table height.
Credits

This module would not be possible without the Tablesaw plugin itself. Thanks to zachleat for making it available and to all the others who helped inspire it.


Thanks